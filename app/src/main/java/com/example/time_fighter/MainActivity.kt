package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var  welcomeMessageTextView: TextView
    private lateinit var  welcomeSubtitleTextView: TextView
    private lateinit var  changeTextButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        welcomeMessageTextView = findViewById(R.id.welcome_message)
        welcomeSubtitleTextView = findViewById(R.id.welcome_subtitle)
        changeTextButton = findViewById((R.id.welcome_button))


        changeTextButton.setOnClickListener{
            this.changeMessageAndSubtitle()

        }

    }

    private fun changeMessageAndSubtitle(){
        welcomeMessageTextView.text= getString(R.string.welcom_message)
        welcomeMessageTextView.text= getString(R.string.welcom_mensaje)

    }
}
